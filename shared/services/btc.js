import axios from "axios";

const instance = axios.create({
  baseURL: "https://api.coindesk.com/v1",
});

export const getBtcPrice = async () => {
  const { data } = await instance.get("/bpi/currentprice.json");

  return data;
};
