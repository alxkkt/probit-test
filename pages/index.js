/* eslint-disable @next/next/no-page-custom-font */
import Head from "next/head";
import styles from "../styles/Home.module.scss";
import "../node_modules/modern-normalize/modern-normalize.css";

import BtcSection from "../components/BtcSection";
import HeroSection from "../components/HeroSection";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Coin Price</title>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500&display=swap"
          rel="stylesheet"
        />
      </Head>

      <main className={styles.container}>
        <HeroSection />
        <BtcSection />
      </main>
    </div>
  );
}
