import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

const initialState = {
  currentUSDPrice: null,
  currentEURPrice: null,
  currentGBPPrice: null,
};

export const btcSlice = createSlice({
  name: "btc",
  initialState,
  reducers: {
    setCurrentUSDPrice(state, action) {
      state.currentUSDPrice = action.payload;
    },

    setCurrentEURPrice(state, action) {
      state.currentEURPrice = action.payload;
    },

    setCurrentGBPPrice(state, action) {
      state.currentGBPPrice = action.payload;
    },

    extraReducers: {
      [HYDRATE]: (state, action) => {
        return {
          ...state,
          ...action.payload.btc,
        };
      },
    },
  },
});

export const { setCurrentUSDPrice, setCurrentEURPrice, setCurrentGBPPrice } =
  btcSlice.actions;

export default btcSlice.reducer;
