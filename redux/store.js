import { configureStore } from "@reduxjs/toolkit";
import { btcSlice } from "./btc/btc-slice";
import { createWrapper } from "next-redux-wrapper";

const makeStore = () =>
  configureStore({
    reducer: {
      [btcSlice.name]: btcSlice.reducer,
    },
    devTools: true,
  });

export const wrapper = createWrapper(makeStore);
