import { useEffect, useState } from "react";
import {
  setCurrentUSDPrice,
  setCurrentEURPrice,
  setCurrentGBPPrice,
} from "../../redux/btc/btc-slice";
import { useDispatch, useSelector } from "react-redux";
import { getBtcPrice } from "../../shared/services/btc";
import { selectBtcState } from "../../redux/btc/btc-selectors";

import styles from "./BtcSection.module.scss";

export default function BtcSection() {
  const btcState = useSelector(selectBtcState);
  const dispatch = useDispatch();

  const fetchData = async () => {
    const result = await getBtcPrice();

    dispatch(setCurrentUSDPrice(result.bpi.USD.rate.slice(0, -2)));
    dispatch(setCurrentEURPrice(result.bpi.EUR.rate.slice(0, -2)));
    dispatch(setCurrentGBPPrice(result.bpi.GBP.rate.slice(0, -2)));
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      {/* <h2>Current BTC Price is: {btcState.currentUSDPrice}</h2> */}
      <label htmlFor="coins">Current BTC Price is:</label>

      <select name="coins" id="coins" className={styles.selectEl}>
        <option value="USD">$ {btcState.currentUSDPrice}</option>
        <option value="EUR">€ {btcState.currentEURPrice}</option>
        <option value="GBP">£ {btcState.currentGBPPrice}</option>
      </select>
    </div>
  );
}
