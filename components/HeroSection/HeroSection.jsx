import Image from "next/image";
import heroImg from "../../public/hero.png";

import styles from "./HeroSection.module.scss";

export default function HeroSection() {
  return (
    <section className={styles.HeroSection}>
      <h1 className={styles.sign}>
        Keep up with the latest cryptocurrency price using our app
      </h1>
      <Image src={heroImg} alt="hero" width={600} height={550} />
    </section>
  );
}
